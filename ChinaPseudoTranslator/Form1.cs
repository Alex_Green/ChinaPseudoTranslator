﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ChinaPseudoTranslator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void textBox_IN_TextChanged(object sender, EventArgs e)
        {
            string Text = textBox_IN.Text.ToLowerInvariant();

            // Russian
            Text = Text.Replace("а", "丹");
            Text = Text.Replace("б", "五");
            Text = Text.Replace("в", "乃");
            Text = Text.Replace("г", "厂");
            Text = Text.Replace("д", "亼");
            Text = Text.Replace("е", "仨");
            Text = Text.Replace("ё", "庄");
            Text = Text.Replace("ж", "水");
            Text = Text.Replace("з", "了");
            Text = Text.Replace("и", "仈");
            Text = Text.Replace("й", "认");
            Text = Text.Replace("к", "长");
            Text = Text.Replace("л", "几");
            Text = Text.Replace("м", "从");
            Text = Text.Replace("н", "廾");
            Text = Text.Replace("о", "口");
            Text = Text.Replace("п", "冂");
            Text = Text.Replace("р", "尸");
            Text = Text.Replace("с", "仁");
            Text = Text.Replace("т", "丁");
            Text = Text.Replace("у", "丫");
            Text = Text.Replace("ф", "中");
            Text = Text.Replace("х", "乂");
            Text = Text.Replace("ц", "凵");
            Text = Text.Replace("ч", "丩");
            Text = Text.Replace("ш", "山");
            Text = Text.Replace("щ", "山");
            Text = Text.Replace("ь", "乚");
            Text = Text.Replace("ы", "科");
            Text = Text.Replace("ъ", "乙");
            Text = Text.Replace("э", "彐");
            Text = Text.Replace("ю", "扣");
            Text = Text.Replace("я", "牙");

            // Ukrainian
            Text = Text.Replace("ґ", "广");
            Text = Text.Replace("ї", "生");
            Text = Text.Replace("і", "工");
            Text = Text.Replace("є", "仨");

            // English
            Text = Text.Replace("a", "丹");
            Text = Text.Replace("b", "归");
            Text = Text.Replace("c", "仁");
            Text = Text.Replace("d", "力");
            Text = Text.Replace("e", "仨");
            Text = Text.Replace("f", "下");
            Text = Text.Replace("g", "马");
            Text = Text.Replace("h", "卄");
            Text = Text.Replace("i", "工");
            Text = Text.Replace("j", "亅");
            Text = Text.Replace("k", "长");
            Text = Text.Replace("l", "乚");
            Text = Text.Replace("m", "从");
            Text = Text.Replace("n", "刃");
            Text = Text.Replace("o", "囗");
            Text = Text.Replace("p", "尸");
            Text = Text.Replace("q", "贝");
            Text = Text.Replace("r", "尺");
            Text = Text.Replace("s", "丂");
            Text = Text.Replace("t", "丁");
            Text = Text.Replace("u", "凵");
            Text = Text.Replace("v", "丫");
            Text = Text.Replace("w", "屮");
            Text = Text.Replace("x", "乂");
            Text = Text.Replace("y", "丫");
            Text = Text.Replace("z", "乙");

            textBox_Out.Text = Text;
        }
    }
}
