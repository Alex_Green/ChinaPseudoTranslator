﻿namespace ChinaPseudoTranslator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox_IN = new System.Windows.Forms.TextBox();
            this.textBox_Out = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.textBox_Out, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox_IN, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(624, 442);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // textBox_IN
            // 
            this.textBox_IN.AcceptsReturn = true;
            this.textBox_IN.AcceptsTab = true;
            this.textBox_IN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_IN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_IN.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_IN.Location = new System.Drawing.Point(3, 3);
            this.textBox_IN.Multiline = true;
            this.textBox_IN.Name = "textBox_IN";
            this.textBox_IN.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_IN.Size = new System.Drawing.Size(618, 215);
            this.textBox_IN.TabIndex = 0;
            this.textBox_IN.TextChanged += new System.EventHandler(this.textBox_IN_TextChanged);
            // 
            // textBox_Out
            // 
            this.textBox_Out.AcceptsReturn = true;
            this.textBox_Out.AcceptsTab = true;
            this.textBox_Out.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_Out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_Out.Font = new System.Drawing.Font("Microsoft JhengHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Out.Location = new System.Drawing.Point(3, 224);
            this.textBox_Out.Multiline = true;
            this.textBox_Out.Name = "textBox_Out";
            this.textBox_Out.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_Out.Size = new System.Drawing.Size(618, 215);
            this.textBox_Out.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 442);
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "China Pseudo Translator =)";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBox_Out;
        private System.Windows.Forms.TextBox textBox_IN;
    }
}

