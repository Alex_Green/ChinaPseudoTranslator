﻿# ChinaPseudoTranslator

## Description
ChinaPseudoTranslator is a FREE, OpenSource tool for replacing symbols of EN, RU, UA to CH alphabets.

## Example

> HELLO WORLD - 卄仨乚乚囗 屮囗尺乚力

> ПРИВЕТ МИР  - 冂尸仈乃仨丁 从仈尸

> ПРИВІТ СВІТ - 冂尸仈乃工丁 仁乃工丁

## License:
**MIT**

## Authors
* Zelenskyi Alexandr (Зеленський Олександр)